package com.project.mapper.transport;

import com.project.mapper.parking.ParkingNameMapper;
import com.project.model.dto.TransportDto;
import com.project.model.entity.Transport;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = ParkingNameMapper.class)
public interface TransportMapper {
    Transport dtoToModel(TransportDto transportDto);
    TransportDto modelToDto(Transport transport);
}