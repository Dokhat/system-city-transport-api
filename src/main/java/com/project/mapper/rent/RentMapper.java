package com.project.mapper.rent;

import com.project.model.dto.RentDto;
import com.project.model.entity.Rent;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RentMapper {
    RentDto modelToDto(Rent rent);
}
