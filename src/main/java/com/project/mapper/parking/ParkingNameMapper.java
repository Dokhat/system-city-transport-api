package com.project.mapper.parking;

import com.project.model.dto.transport.ParkingNameDto;
import com.project.model.entity.Parking;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ParkingNameMapper {
    ParkingNameDto modelToDto(Parking parking);
}