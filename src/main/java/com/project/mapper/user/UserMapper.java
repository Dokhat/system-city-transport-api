package com.project.mapper.user;

import com.project.model.dto.UserDto;
import com.project.model.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserMapper {
    UserDto modelToDto(User user);
}
