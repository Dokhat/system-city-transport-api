package com.project.model.entity;

public enum TransportType {
    BICYCLE, SCOOTER
}
