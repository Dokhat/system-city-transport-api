package com.project.model.entity;

public enum RentStatus {
    OPEN, CLOSE
}
