package com.project.model.entity;

public enum TransportStatus {
    FREE, BUSY, UNAVAILABLE
}
