package com.project.model.dto.error;

import com.project.service.ErrorCodeEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorDto {
    private ErrorCodeEnum errorCode;
    private String message;

    public ErrorDto(ErrorCodeEnum errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }
}
