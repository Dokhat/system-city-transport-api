package com.project.model.dto;

import com.project.model.entity.RentStatus;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.sql.Time;
import java.sql.Timestamp;

@Getter
@Setter
public class RentDto {
    private Long id;
    @Valid
    private TransportDto transport;
    private UserDto user;
    @NotNull
    private Timestamp beginTimeRent;
    private Timestamp endTimeRent;
    @Valid
    private ParkingDto beginParking;
    private ParkingDto endParking;
    @NotNull
    private RentStatus status;
    private Long amount;
}
