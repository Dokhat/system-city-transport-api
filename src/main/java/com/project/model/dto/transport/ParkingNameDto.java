package com.project.model.dto.transport;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ParkingNameDto {
    private String name;
}
