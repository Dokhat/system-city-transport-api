package com.project.model.dto;

import com.project.model.dto.transport.ParkingNameDto;
import com.project.model.entity.Condition;
import com.project.model.entity.TransportStatus;
import com.project.model.entity.TransportType;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

@Getter
@Setter
public class TransportDto {
    private Long id;
    @NotNull(message = "Тип не может быть пустой")
    private TransportType type;
    private String identificationNumber;
    private String coordinates;
    private Condition condition;
    private TransportStatus status;
    private Long chargePercentage;
    private Long maxSpeed;
    private ParkingNameDto parking;
}