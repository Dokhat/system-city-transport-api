package com.project.repository;

import com.project.model.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long>{
    @Query("select u from User u where u.login = :login")
    User findByLogin(String login);
}
